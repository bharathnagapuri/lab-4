// CPP Program to implement merge_p sort using 
// multi-threading 
#include <iostream> 
#include <pthread.h> 
#include <time.h> 
  
// number of elements in array 
#define MAX 20 
  
// number of threads 
#define THREAD_MAX 4 
  
using namespace std; 
  
// array of size MAX 
int a[MAX], b[MAX]; 
int part = 0; 
  
// Merges two subarrays of arr[]. 
// First subarray is arr[l..m] 
// Second subarray is arr[m+1..r] 
void merge(int arr[], int l, int m, int r) 
{ 
    int i, j, k; 
    int n1 = m - l + 1; 
    int n2 =  r - m; 
  
    /* create temp arrays */
    int L[n1], R[n2]; 
  
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1+ j]; 
  
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray 
    j = 0; // Initial index of second subarray 
    k = l; // Initial index of merged subarray 
    while (i < n1 && j < n2) 
    { 
        if (L[i] <= R[j]) 
        { 
            arr[k] = L[i]; 
            i++; 
        } 
        else
        { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
} 
  
/* l is for left index and r is right index of the 
   sub-array of arr to be sorted */
void mergeSort(int arr[], int l, int r) 
{ 
    if (l < r) 
    { 
        // Same as (l+r)/2, but avoids overflow for 
        // large l and h 
        int m = l+(r-l)/2; 
  
        // Sort first and second halves 
        mergeSort(arr, l, m); 
        mergeSort(arr, m+1, r); 
  
        merge(arr, l, m, r); 
    } 
} 




// merge_p function for merging two parts 
void merge_p(int low, int mid, int high) 
{ 
    int* left = new int[mid - low + 1]; 
    int* right = new int[high - mid]; 
  
    // n1 is size of left part and n2 is size 
    // of right part 
    int n1 = mid - low + 1, n2 = high - mid, i, j; 
  
    // storing values in left part 
    for (i = 0; i < n1; i++) 
        left[i] = a[i + low]; 
  
    // storing values in right part 
    for (i = 0; i < n2; i++) 
        right[i] = a[i + mid + 1]; 
  
    int k = low; 
    i = j = 0; 
  
    // merge_p left and right in ascending order 
    while (i < n1 && j < n2) { 
        if (left[i] <= right[j]) 
            a[k++] = left[i++]; 
        else
            a[k++] = right[j++]; 
    } 
  
    // insert remaining values from left 
    while (i < n1) { 
        a[k++] = left[i++]; 
    } 
  
    // insert remaining values from right 
    while (j < n2) { 
        a[k++] = right[j++]; 
    } 
} 
  
// merge_p sort function 
void merge_sort_p(int low, int high) 
{ 
    // calculating mid point of array 
    int mid = low + (high - low) / 2; 
    if (low < high) { 
  
        // calling first half 
        merge_sort_p(low, mid); 
  
        // calling second half 
        merge_sort_p(mid + 1, high); 
  
        // merging the two halves 
        merge_p(low, mid, high); 
    } 
} 
  
// thread function for multi-threading 
void* merge_sort_p(void* arg) 
{ 
    // which part out of 4 parts 
    int thread_part = part++; 
  
    // calculating low and high 
    int low = thread_part * (MAX / 4); 
    int high = (thread_part + 1) * (MAX / 4) - 1; 
  
    // evaluating mid point 
    int mid = low + (high - low) / 2; 
    if (low < high) { 
        merge_sort_p(low, mid); 
        merge_sort_p(mid + 1, high); 
        merge_p(low, mid, high); 
    } 
} 
  
// Driver Code 
int main() 
{ 
    // generating random values in array 
    for (int i = 0; i < MAX; i++) 
    {
       a[i] = rand() % 100;
       b[i] = a[i];
    } 

    cout << "unorted array: "; 
    for (int i = 0; i < MAX; i++) 
        cout << b[i] << " "; 
    cout<<endl;

    clock_t t3, t4;
    
    t3=clock();
    mergeSort(b, 0, MAX-1);
    t4=clock();

    // displaying sorted array 
    cout << "Sorted array: "; 
    for (int i = 0; i < MAX; i++) 
        cout << b[i] << " "; 
  
    // time taken by merge_p sort in seconds 
    cout << "Seq Time taken: " << (t4 - t3) /  
              (double)CLOCKS_PER_SEC << endl; 


    // t1 and t2 for calculating time for 
    // merge_p sort parallel
    clock_t t1, t2; 
  
    t1 = clock(); 
    pthread_t threads[THREAD_MAX]; 
  
    // creating 4 threads 
    for (int i = 0; i < THREAD_MAX; i++) 
        pthread_create(&threads[i], NULL, merge_sort_p, 
                                        (void*)NULL); 
  
    // joining all 4 threads 
    for (int i = 0; i < 4; i++) 
        pthread_join(threads[i], NULL); 
  
    // merging the final 4 parts 
    merge_p(0, (MAX / 2 - 1) / 2, MAX / 2 - 1); 
    merge_p(MAX / 2, MAX/2 + (MAX-1-MAX/2)/2, MAX - 1); 
    merge_p(0, (MAX - 1)/2, MAX - 1); 
  
    t2 = clock(); 
  
    // displaying sorted array 
    cout << "Sorted array: "; 
    for (int i = 0; i < MAX; i++) 
        cout << a[i] << " "; 
  
    // time taken by merge_p sort in seconds 
    cout << "Parallel Time taken: " << (t2 - t1) /  
              (double)CLOCKS_PER_SEC << endl; 
  
    return 0; 
}